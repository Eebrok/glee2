using System;
using System.Linq;
using System.Threading.Tasks;
using Avenue.Domain;
using Avenue.Events;
using Data.Projections.ExceptionHandling;
using Domain.Employees.Events;

namespace Data.Projections.EventHandlers
{
    public class EmployeeListingEventHandlers : IEventHandler<EmployeeCreated>, IEventHandler<EmployeeNamesChanged>,
        IEventHandler<EmployeeDisplayNameChanged>, IEventHandler<EmployeeAddressChanged>,
        IEventHandler<EmployeePhoneNumberChanged>, IEventHandler<EmployeeCompanyEmailChanged>,
        IEventHandler<EmployeePersonalEmailChanged>, IEventHandler<EmployeeTagsChanged>,
        IEventHandler<EmployeeRemoved>,IEventHandler<EmployeeBirthDateChanged>,IEventHandler<EmployeeEffectiveDateChanged>,
        IEventHandler<EmployeeSalaryChanged>,IEventHandler<EmployeeSalaryTypeChanged>,
        IEventHandler<EmployeeIsActiveChanged>
    {
        readonly IWritableRepository<EmployeeListing> _repository;

        public EmployeeListingEventHandlers(IWritableRepository<EmployeeListing> repository)
        {
            _repository = repository;
        }

        public async Task Handle(EmployeeCreated @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeCreated event can't be null");
            await _repository.Create(new EmployeeListing(@event.EmployeeId, @event.FirstName, @event.MiddleName,
                @event.LastName, @event.SecondLastName, @event.DisplayName, @event.CompanyEmail, @event.PersonalEmail,
                @event.Birthdate, @event.StartDate, @event.Address, @event.PhoneNumber, @event.BankName,
                @event.AccountNumber, @event.Gender, @event.Tags, @event.Country, @event.Region, @event.City,
                @event.Salary, @event.SalaryType, @event.EffectiveDate, true));
        }

        public async Task Handle(EmployeeNamesChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeNamesChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeeDisplayNameChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeDisplayNameChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        async Task ModifyEmployeeListing(object @event, Guid eventEmployeeId)
        {
            var existing = await _repository.Find(eventEmployeeId);
            CopyValues(@event, existing);
            await _repository.Update(existing);
        }

        static void CopyValues(object @event, EmployeeListing existing)
        {
            var eventPropertyInfos = @event.GetType().GetProperties();
            var employeePropertyInfos = existing.GetType().GetProperties();
            foreach (var propertyInfo in eventPropertyInfos)
            {
                var matchingProperty = employeePropertyInfos.FirstOrDefault(x => x.Name == propertyInfo.Name);
                if (matchingProperty == null) 
                    continue;
                if (!matchingProperty.CanWrite)
                {
                    throw new Exception($"{propertyInfo.Name} does not have a setter.");
                }
                matchingProperty.SetValue(existing, propertyInfo.GetValue(@event));
            }
        }

        public async Task Handle(EmployeeAddressChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeAddressChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeePhoneNumberChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeePhoneNumberChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeeCompanyEmailChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeCompanyEmailChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeePersonalEmailChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeePersonalEmailChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeeTagsChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeTagsChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }
        
        public async Task Handle(EmployeeSalaryTypeChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeSalaryTypeChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeeSalaryChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeSalaryChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeeEffectiveDateChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeEffectiveDateChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeeBirthDateChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeBirthDateChanged event can't be null");
            await ModifyEmployeeListing(@event, @event.EmployeeId);
        }

        public async Task Handle(EmployeeRemoved @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeRemoved event can't be null");
            var existing = await _repository.Find(@event.EmployeeId);
            existing.Remove();
            await _repository.Update(existing);
        }

        public async Task Handle(EmployeeIsActiveChanged @event)
        {
            if(@event==null)
                throw new EventNullException("EmployeeIsActiveChanged event can't be null");
            var existing = await _repository.Find(@event.EmployeeId);
            existing.IsActive = @event.IsActive;
            await _repository.Update(existing);
        }
    }
}