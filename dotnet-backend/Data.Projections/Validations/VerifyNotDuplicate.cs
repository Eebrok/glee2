using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;
using Domain.Common;
using Domain.Employees;
using Domain.Employees.Commands;
using FluentValidation.Results;

namespace Data.Projections.Validations
{
    public class VerifyNotDuplicate : ICommandValidator<CreateEmployee>
    {
        //EE7: START
        readonly IReadOnlyRepository<EmployeeListing> _readOnlyRepository;
        
         public VerifyNotDuplicate(IReadOnlyRepository<EmployeeListing> readOnlyRepository)
         {
             _readOnlyRepository = readOnlyRepository;
         }
        //EE7:END
        //RP7: START
        // readonly IReadOnlyRepository<Employee> _readOnlyRepository;
        // public VerifyNotDuplicate(IReadOnlyRepository<Employee> readOnlyRepository)
        // {
        //     _readOnlyRepository = readOnlyRepository;
        // }
        //RP7: END
        
        public Task Validate(CreateEmployee command)
        {
            var matches = _readOnlyRepository.Set()
                .Where(x =>
                    x.FirstName == command.FirstName && x.MiddleName == command.MiddleName &&
                    x.LastName == command.LastName && x.SecondLastName == command.SecondLastName);

            if (matches.Any())
                throw new CommandValidationException<CreateEmployee>(new List<ValidationFailure>
                    {new ValidationFailure("Name", "Employee with that name already exists.",
                        string.Join(" ",
                            new[] {command.FirstName, command.MiddleName, command.LastName, command.SecondLastName}))});

            return Task.CompletedTask;
        }
    }
}