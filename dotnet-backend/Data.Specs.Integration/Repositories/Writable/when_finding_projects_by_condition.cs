using System;
using System.Collections.Generic;
using System.Linq;
using AcklenAvenue.Events;
using Data.Repositories;
using Domain.Employees;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Specs.Integration.Repositories.Writable
{
    public class when_finding_projects_by_condition
    {
        static ReadOnlyRepository<Employee, Guid> _systemUnderTest;
        static IEnumerable<Employee> _result;
        static string _lastName;
        static Employee _expectedEmployee;
        static IEventDispatcher _eventDispatcher;
        static AppDataContext _appDataContext;

        Establish _context = () =>
        {
            _appDataContext = InMemoryDataContext.GetInMemoryContext().Seed();

            _eventDispatcher = Mock.Of<IEventDispatcher>();
            _systemUnderTest = new ReadOnlyRepository<Employee, Guid>(_appDataContext);

            _expectedEmployee = _appDataContext.Set<Employee>().First();

            _lastName = _expectedEmployee.LastName;
        };

        Because of = () => { _result = _systemUnderTest.Find(new Dictionary<string, string>{{"LastName", _lastName}}); };

        It should_return_the_matching_projects = () => { _result.Should().ContainEquivalentOf(_expectedEmployee); };
        
        Cleanup after = () => { _appDataContext.Clean(); };
    }
}