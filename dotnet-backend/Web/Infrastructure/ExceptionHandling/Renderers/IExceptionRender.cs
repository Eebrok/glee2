using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Web.Infrastructure.ExceptionHandling.Renderers
{
    public interface IExceptionRender
    {
        bool ShouldHandle(Exception exception);
        Task Render(HttpContext context, Exception exception);
    }
}