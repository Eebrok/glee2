using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Web.Infrastructure.Bootstrapping
{
    public static class SwaggerBootstrapper
    {
        public static IServiceCollection ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(swagger =>
            {
                var contact = new Contact
                    {Name = SwaggerConfiguration.ContactName, Url = SwaggerConfiguration.ContactUrl};
                swagger.SwaggerDoc(SwaggerConfiguration.DocNameV1,
                    new Info
                    {
                        Title = SwaggerConfiguration.DocInfoTitle,
                        Version = SwaggerConfiguration.DocInfoVersion,
                        Description = SwaggerConfiguration.DocInfoDescription,
                        Contact = contact
                    }
                );
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                swagger.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    In = "header",
                    Description = "JWT Authorization header using the Bearer scheme. Input should be: \" Bearer {token}\"",
                    Name = "Authorization",
                    Type = "apiKey"
                });
                swagger.AddSecurityRequirement(security);
                swagger.EnableAnnotations();
            });
            return services;
        }

        public static IApplicationBuilder SetupAppForDevelopmentMode(this IApplicationBuilder app)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
            });

            return app;
        }
    }
}