using System;
using System.Text;
using dotenv.net.DependencyInjection.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Infrastructure.Bootstrapping
{
    public static class EnvironmentBootstrappingExtensions
    {
        public static bool IsDevelopment(this IConfiguration config)
        {
            if(config == null)
                throw new ArgumentNullException(nameof(config));
            var isDevelopment = config["ENVIRONMENT"] == "development";
            return isDevelopment;
        }

        public static IServiceCollection ConfigureEnvironmentVariables(this IServiceCollection services)
        {
            services.AddEnv(builder =>
            {
                builder
                    .AddEnvFile(".env")
                    .AddThrowOnError(false)
                    .AddEncoding(Encoding.ASCII);
            });
            return services;
        }
    }
}