using System;
using Domain.Common.Services;
using Microsoft.Extensions.DependencyInjection;
using Services;
using Web.Infrastructure.ExceptionHandling.Renderers;
using Web.Status;

namespace Web.Infrastructure.Bootstrapping.Dependencies
{
    public static class WebDependencyBootstrapper
    {
        public static IServiceCollection ConfigureWebDependencies(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<IExceptionRender>()
                    .AddClasses(c => c.AssignableTo<IExceptionRender>())
                    .AsImplementedInterfaces().WithSingletonLifetime()
            );
            return services;
        }

        public static IServiceCollection ConfigureAuxiliaryServices(this IServiceCollection services)
        {
            services.AddSingleton<IIdentityGenerator<Guid>, GuidIdentityGenerator>();
            services.AddSingleton<IApiInformationGetter, DefaultApiInformationGetter>();
            return services;
        }
    }
}