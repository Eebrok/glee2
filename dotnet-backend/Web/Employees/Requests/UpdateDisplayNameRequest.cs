namespace Web.Employees.Requests
{
    public class UpdateDisplayNameRequest
    {
        public string DisplayName { get; set; } = "";
    }
}