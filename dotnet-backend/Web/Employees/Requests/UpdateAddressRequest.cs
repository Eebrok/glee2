namespace Web.Employees.Requests
{
    public class UpdateAddressRequest
    {
        public string Address { get; set; } = "";
        public string Country { get; set; } = "";
        public string Region { get; set; } = "";
        public string City { get; set; } = "";
    }
}