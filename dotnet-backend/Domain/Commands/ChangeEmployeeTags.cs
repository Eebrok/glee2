using System;

namespace Domain.Commands
{
    public class ChangeEmployeeTags
    {
        public Guid Id { get; }
        public string Tags { get; }

        public ChangeEmployeeTags(Guid id, string tags)
        {
            Id = id;
            Tags = tags;
        }
    }
}