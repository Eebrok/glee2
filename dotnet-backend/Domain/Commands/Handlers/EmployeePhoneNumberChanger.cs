using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeePhoneNumberChanger : ICommandHandler<ChangeEmployeePhoneNumber>
    {
        private readonly IWritableRepository<Employee, Guid> _writableRepository;

        public EmployeePhoneNumberChanger(IWritableRepository<Employee,Guid> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeePhoneNumber command)
        {
            var employee = await _writableRepository.GetById(command.Id);
            employee.ChangePhoneNumber(command.PhoneNumber);

            await _writableRepository.Update(employee);
        }
    }
}