using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeeDisplayNameChanger : ICommandHandler<ChangeEmployeeDisplayName>
    {
        readonly IWritableRepository<Employee, Guid> _writableRepository;

        public EmployeeDisplayNameChanger(IWritableRepository<Employee, Guid> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeeDisplayName command)
        {
            var employee = await _writableRepository.GetById(command.Id);
            employee.ChangeDisplayName(command.DisplayName);
            await _writableRepository.Update(employee);
        }
    }
}