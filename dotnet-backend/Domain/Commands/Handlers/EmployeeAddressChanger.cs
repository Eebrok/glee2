using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeeAddressChanger : ICommandHandler<ChangeEmployeeAddress>
    {
        private readonly IWritableRepository<Employee, Guid> _writableRepository;

        public EmployeeAddressChanger(IWritableRepository<Employee,Guid> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeeAddress command)
        {
            var employee = await _writableRepository.GetById(command.Id);
            employee.ChangeAddress(command.Address, command.City, command.Region, command.Country);
            await _writableRepository.Update(employee);
        }
    }
}