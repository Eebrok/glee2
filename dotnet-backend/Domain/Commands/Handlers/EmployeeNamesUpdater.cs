using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeeNamesUpdater : ICommandHandler<ChangeEmployeeNames>
    {
        readonly IWritableRepository<Employee, Guid> _writableRepository;

        public EmployeeNamesUpdater(IWritableRepository<Employee, Guid> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeeNames command)
        {
            var employee = await _writableRepository.GetById(command.Id);
            employee.ChangeNames(command.FirstName, command.MiddleName, command.LastName, command.SecondLastName);
            await _writableRepository.Update(employee);
        }
    }
}