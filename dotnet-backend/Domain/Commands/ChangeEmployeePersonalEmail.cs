using System;

namespace Domain.Commands
{
    public class ChangeEmployeePersonalEmail
    {
        public Guid Id { get; }
        public string PersonalEmail { get; }

        public ChangeEmployeePersonalEmail(Guid id, string personalEmail)
        {
            Id = id;
            PersonalEmail = personalEmail;
        }
    }
}