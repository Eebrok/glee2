using System;
using Common.Entities;

namespace Domain.Events
{
    public class EmployeePersonalEmailChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string NewPersonalEmail { get; }

        public EmployeePersonalEmailChanged(Guid employeeId, string newPersonalEmail)
        {
            EmployeeId = employeeId;
            NewPersonalEmail = newPersonalEmail;
        }
    }
}