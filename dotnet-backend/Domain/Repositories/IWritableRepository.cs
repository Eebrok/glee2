using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IWritableRepository
    {
        
    }
    
    public interface IWritableRepository<T, TPKey> : IWritableRepository, IReadOnlyRepository<T, TPKey>
    {
        Task<T> Create(T entity);
        Task<T> Update(T entity);
    }
}