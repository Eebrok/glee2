using System;
using FluentAssertions;
using Machine.Specifications;
using It = Machine.Specifications.It;

namespace Data.Projections.Specs
{
    public class when_creating_new_employee_listing
    {
        Establish _context = () =>
        {
        };

        Because of = () => { _result = new EmployeeListing(
            new Guid(),
            "test",
            "test",
            "Testing",
            "Testing",
            "Test Testing",
            "test@test.com",
            "",
            DateTime.Today,
            null,
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            null,
            "",
            null,
            true); };

        It should_exist_a_new_listing = () => { _result.DisplayName.Should().Be("Test Testing"); };
        static EmployeeListing _result;
    }
}