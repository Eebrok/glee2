using System;
using System.Threading.Tasks;
using Avenue.Domain;
using Data.Projections.EventHandlers;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using FluentAssertions.Common;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Projections.Specs
{
    public class given_handler_context
    {
        Establish _context = () =>
        {
            _writableRepository = new Mock<IWritableRepository<EmployeeListing>>();
            _systemUnderTest = new EmployeeListingEventHandlers(_writableRepository.Object);
        };

        protected static Mock<IWritableRepository<EmployeeListing>> _writableRepository;
        protected static EmployeeListingEventHandlers _systemUnderTest;
    }

    public class given_update_handler_context : given_handler_context
    {
        Establish _context = () =>
        {
            _existingEmployee = Builder<EmployeeListing>.CreateNew().Build();
            _writableRepository.Setup(moq 
                => moq.Find(Moq.It.IsAny<Guid>())).ReturnsAsync(_existingEmployee);
            _writableRepository.Setup(moq =>
                    moq.Update(Moq.It.IsAny<EmployeeListing>()))
                .Callback<EmployeeListing>(obj=>_argumentListing = obj);
        };

        static EmployeeListing _existingEmployee;
        protected static EmployeeListing _argumentListing;
    }
    public class when_handling_employee_created_event : given_handler_context
    {
        Establish _context = () =>
        {
            _employeeCreated = Builder<EmployeeCreated>.CreateNew().Build();
            _expectedEmployeeListing = new EmployeeListing(_employeeCreated.EmployeeId, _employeeCreated.FirstName,
                _employeeCreated.MiddleName, _employeeCreated.LastName, _employeeCreated.SecondLastName,
                _employeeCreated.DisplayName, _employeeCreated.CompanyEmail, _employeeCreated.PersonalEmail,
                _employeeCreated.Birthdate, _employeeCreated.StartDate, _employeeCreated.Address,
                _employeeCreated.PhoneNumber, _employeeCreated.BankName, _employeeCreated.AccountNumber,
                _employeeCreated.Gender, _employeeCreated.Tags, _employeeCreated.Country, _employeeCreated.Region,
                _employeeCreated.City, _employeeCreated.Salary, _employeeCreated.SalaryType,
                _employeeCreated.EffectiveDate, true);
            
            _writableRepository
                .Setup(x => x.Create(Moq.It.IsAny<EmployeeListing>()))
                .Callback<EmployeeListing>(obj=>_argumentListing=obj);
        };

        Because of = () => { _systemUnderTest.Handle(_employeeCreated); };

        It should_call_the_repository_with_new_client_data = () =>
            {
                _systemUnderTest.Handle(_employeeCreated);
                _writableRepository.Verify(x=>x.Create(Moq.It.IsAny<EmployeeListing>()));
                _argumentListing.Should().BeEquivalentTo(_expectedEmployeeListing);
            };
        static EmployeeCreated _employeeCreated;
        static EmployeeListing _expectedEmployeeListing;
        static EmployeeListing _argumentListing;
    }
}