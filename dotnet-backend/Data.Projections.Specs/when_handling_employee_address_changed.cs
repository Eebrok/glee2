using System;
using Domain.Employees.Events;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Projections.Specs
{
    public class when_handling_employee_address_changed : given_update_handler_context
    {
        Establish _context = () =>
        {
            _employeeAddressChanged = new EmployeeAddressChanged(
                Guid.NewGuid(),
                "Test",
                "Test",
                "Test",
                "Test");
        };

        Because of = () => { _systemUnderTest.Handle(_employeeAddressChanged); };

        It should_call_update_repository_with_new_employee_structure = () =>
        {
            _writableRepository.Verify(repo=>repo.Update(Moq.It.IsAny<EmployeeListing>()));
            _argumentListing.Address.Should().Be("Test");
            _argumentListing.City.Should().Be("Test");
            _argumentListing.Region.Should().Be("Test");
            _argumentListing.Country.Should().Be("Test");
        };

        static EmployeeAddressChanged _employeeAddressChanged;
    }
}