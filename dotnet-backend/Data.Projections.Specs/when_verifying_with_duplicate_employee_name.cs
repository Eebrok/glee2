using System;
using System.Collections.Generic;
using System.Linq;
using Avenue.Domain;
using Avenue.Testing.EntityFramework;
using Data.Projections.Validations;
using Domain.Common;
using Domain.Employees;
using Domain.Employees.Commands;
using FizzWare.NBuilder;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Projections.Specs
{
    public class when_verifying_with_duplicate_employee_name
    {
        Establish _context = () =>
        {
            //EE8: START
            _readOnlyRepository = Mock.Of<IReadOnlyRepository<EmployeeListing>>();
            //EE8: END
            //RP8: START
            //_readOnlyRepository = Mock.Of<IReadOnlyRepository<Employee>>();
            //RP8: END
            _systemUnderTest = new VerifyNotDuplicate(_readOnlyRepository);

            var employeeId = Guid.NewGuid();

            _command = Builder<CreateEmployee>.CreateNew()
                .With(x => x.Id, employeeId)
                .With(x => x.FirstName, "Mary")
                .With(x => x.MiddleName, "Catherine")
                .With(x => x.LastName, "Jones")
                .With(x => x.SecondLastName, "Clay")
                .Build();

            //EE10: START
            var employeeListing = Builder<EmployeeListing>.CreateNew()
            //EE10: END
            
            //RP:10 START
            //var employeeListing = Builder<Employee>.CreateNew()
            //RP:10 END
            
                .With(x => x.Id, employeeId)
                .With(x => x.FirstName, _command.FirstName)
                .With(x => x.MiddleName, _command.MiddleName)
                .With(x => x.LastName, _command.LastName)
                .With(x => x.SecondLastName, _command.SecondLastName)
                .Build();

            Mock.Get(_readOnlyRepository)
                .Setup(x => x.Set())
                //EE11: START
                .Returns(new List<EmployeeListing> {employeeListing}.AsAsyncQueryable());
                //EE11: END
                //RP11: START
                //.Returns(new List<Employee> {employeeListing}.AsAsyncQueryable());
                //RP11: END
        };

        Because of = () => { _exception = Catch.Exception(() => _systemUnderTest.Validate(_command).Wait()); };

        It should_throw_mentioning_the_name = () =>
        {
            _exception.Should().BeOfType<CommandValidationException<CreateEmployee>>();

            _exception.As<CommandValidationException<CreateEmployee>>().ValidationFailures.First().Should()
                .BeEquivalentTo(new ValidationFailure("Name", "Employee with that name already exists.",
                    string.Join(" ",
                        new[] {_command.FirstName, _command.MiddleName, _command.LastName, _command.SecondLastName})));
        };

        static VerifyNotDuplicate _systemUnderTest;
        //EE9: START
        static IReadOnlyRepository<EmployeeListing> _readOnlyRepository;
        //EE9: END
        //RP9: START
        //static IReadOnlyRepository<Employee> _readOnlyRepository;
        //RP9: END
        static CreateEmployee _command;
        static Exception _exception;
    }
}