using System.Linq;
using Avenue.Domain;
using Avenue.Testing.Moq;
using Data.Projections.EventHandlers;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Projections.Specs
{
    public class given_an_employee_listing_event_handler_context
    {
        Establish _context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<EmployeeListing>>();
            _systemUnderTest = new EmployeeListingEventHandlers(_writableRepository);
        };

        protected static IWritableRepository<EmployeeListing> _writableRepository;
        protected static EmployeeListingEventHandlers _systemUnderTest;
    }

    public class when_responding_to_a_new_employee : given_an_employee_listing_event_handler_context
    {
        Establish _context = () =>
        {
            _employeeCreated = Builder<EmployeeCreated>.CreateNew().Build();

            _expectedEmployeeListing = new EmployeeListing(_employeeCreated.EmployeeId, _employeeCreated.FirstName,
                _employeeCreated.MiddleName, _employeeCreated.LastName, _employeeCreated.SecondLastName,
                _employeeCreated.DisplayName, _employeeCreated.CompanyEmail, _employeeCreated.PersonalEmail,
                _employeeCreated.Birthdate, _employeeCreated.StartDate, _employeeCreated.Address,
                _employeeCreated.PhoneNumber, _employeeCreated.BankName, _employeeCreated.AccountNumber,
                _employeeCreated.Gender, _employeeCreated.Tags, _employeeCreated.Country, _employeeCreated.Region,
                _employeeCreated.City, _employeeCreated.Salary, _employeeCreated.SalaryType,
                _employeeCreated.EffectiveDate, true);
        };

        Because of = () => { _systemUnderTest.Handle(_employeeCreated).Wait(); };

        It should_create_a_new_listing = () =>
        {
            Mock.Get(_writableRepository).Verify(x => x.Create(
                WithEventHandlersFromSome<EmployeeListing>.Like(_expectedEmployeeListing)));
        };

        static EmployeeCreated _employeeCreated;
        static EmployeeListing _expectedEmployeeListing;
    }
}