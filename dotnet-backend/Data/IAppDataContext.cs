using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public interface IAppDataContext
    {
        DbSet<Employee> Employees { get; set; }    
    }
}