using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_changing_employee_phone_number
    {
        static Employee _employee;

        static string _newPhoneNumber;

        Establish context = () =>
        {
            _newPhoneNumber = "2520-0000";

            _employee = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .With(x => x.PhoneNumber, "2920-0000").Build();
        };

        Because of = () => { _employee.ChangePhoneNumber(_newPhoneNumber); };

        It should_change_the_phone_number = () => { _employee.PhoneNumber.Should().Be(_newPhoneNumber); };

        It should_publish_update_event = () =>
            _employee.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeePhoneNumberChanged(_employee.Id, _newPhoneNumber));
    }
}