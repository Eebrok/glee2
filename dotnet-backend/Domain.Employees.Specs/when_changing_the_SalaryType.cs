using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_changing_the_SalaryType
    {
        static Employee _systemUnderTest;
        static string _newSalaryType;

        Establish _context = () =>
        {
            _newSalaryType = "Type2";
            _systemUnderTest = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .Build();
        };

        Because of = () => { _systemUnderTest.ChangeSalaryType(_newSalaryType); };

        It should_change_the_salary_type = () => { _systemUnderTest.SalaryType.Should().Be(_newSalaryType); };

        It should_notify_the_world_of_the_change = () =>
            _systemUnderTest.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeSalaryTypeChanged(_systemUnderTest.Id, _newSalaryType));
    }
}