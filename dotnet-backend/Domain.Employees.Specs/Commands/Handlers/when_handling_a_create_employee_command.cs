using System;
using Avenue.Domain;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Handlers;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Handlers
{
    [Subject(typeof(EmployeeCreator))]
    public class when_handling_a_create_employee_command
    {
        static IWritableRepository<Employee> _writableRepository;

        static EmployeeCreator _handler;
        static CreateEmployee _createEmployee;

        static Employee _expectedEmployeeToSave;

        Establish context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<Employee>>();
            _handler = new EmployeeCreator(_writableRepository);
            var id = Guid.NewGuid();
            _createEmployee = Builder<CreateEmployee>.CreateNew()
                .With(x => x.Id, id)
                .Build();

            _expectedEmployeeToSave = Builder<Employee>.CreateNew()
                .With(x => x.Id, id)
                .Build();
        };

        Because of = () => _handler.Handle(_createEmployee).Await();

        It should_create_the_employee_in_the_db = () =>
        {
            Mock.Get(_writableRepository).Verify(repository =>
                repository.Create(Moq.It.Is<Employee>(submittedEmployee =>
                    _expectedEmployeeToSave.ShouldBeEquivalent(submittedEmployee))));
        };
    }
}