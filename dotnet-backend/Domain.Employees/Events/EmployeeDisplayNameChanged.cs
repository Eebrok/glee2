using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeDisplayNameChanged : IDomainEvent
    {
        protected EmployeeDisplayNameChanged()
        {
        }

        public Guid EmployeeId { get; } = Guid.Empty;
        public string DisplayName { get; } = "";


        public EmployeeDisplayNameChanged(Guid employeeId, string displayName)
        {
            EmployeeId = employeeId;
            DisplayName = displayName;
        }
    }
}