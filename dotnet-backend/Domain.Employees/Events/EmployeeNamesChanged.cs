using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeNamesChanged : IDomainEvent
    {
        protected EmployeeNamesChanged()
        {
            
        }
        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public string FirstName { get; } = "";
        public string? MiddleName { get; }
        public string LastName { get; } = "";
        public string? SecondLastName { get; } 

        public EmployeeNamesChanged(Guid employeeId, string firstName, string? middleName, string lastName,
            string? secondLastName)
        {
            EmployeeId = employeeId;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            SecondLastName = secondLastName;
        }
    }
}