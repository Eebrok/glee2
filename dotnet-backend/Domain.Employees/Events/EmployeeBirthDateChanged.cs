using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeBirthDateChanged : IDomainEvent
    {
        protected EmployeeBirthDateChanged()
        {
            
        }

        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public DateTime Birthdate { get; set; } = new DateTime();

        public EmployeeBirthDateChanged(Guid id, DateTime birthdate)
        {
            EmployeeId = id;
            Birthdate = birthdate;
        }
    }
}