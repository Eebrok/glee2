using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeePhoneNumberChanged : IDomainEvent
    {
        protected EmployeePhoneNumberChanged()
        {
            
        }

        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public string PhoneNumber { get; } = "";

        public EmployeePhoneNumberChanged(Guid id, string phoneNumber)
        {
            EmployeeId = id;
            PhoneNumber = phoneNumber;
        }
    }
}