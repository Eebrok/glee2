using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeSalaryType : ICommand
    {
        public Guid Id { get; }
        public string SalaryType { get; }

        public ChangeEmployeeSalaryType(Guid id, string salaryType)
        {
            Id = id;
            SalaryType = salaryType;
        }
    }
}