using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeAddressChanger : ICommandHandler<ChangeEmployeeAddress>
    {
        readonly IWritableRepository<Employee> _writableRepository;

        public EmployeeAddressChanger(IWritableRepository<Employee> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeeAddress command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _writableRepository.Find(command.Id);
            employee.ChangeAddress(command.Address, command.City, command.Region, command.Country);
            await _writableRepository.Update(employee);
        }
    }
}