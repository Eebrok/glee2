using System;
using System.Linq;
using Domain.Entities;
using Domain.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Specs.Entities
{
    public class when_changing_employee_tags
    {
        static Employee _employee;

        static string _newTags;

        Establish context = () =>
        {
            _newTags = "Developer";

            _employee = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .With(x => x.Tags, "QA").Build();
        };

        Because of = () => { _employee.ChangeTags(_employee.Id, _newTags); };

        It should_change_the_names = () => { _employee.Tags.Should().Be(_newTags); };

        It should_publish_update_event = () =>
        {
            _employee.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeTagsChanged(_employee.Id, _newTags));
        };
    }
}