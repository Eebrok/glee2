using System;
using System.Linq;
using Domain.Entities;
using Domain.Events;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Specs.Entities
{
    public class when_creating_an_employee
    {
        static Employee _employee;

        Because of = () =>
        {
            _employee = new Employee(Guid.Empty,
                "test", "test",
                "test", "test",
                "test", "test",
                "test", DateTime.Now,
                DateTime.Now, "test",
                "test",
                "test", "test",
                "test",
                "test", "test",
                "test", "test",
                2020,
                DateTime.Now, "test");
        };

        It should_publish_created_event = () =>
        {
            _employee.GetChanges().First().Should().BeOfType<EmployeeCreated>();
        };
    }
}