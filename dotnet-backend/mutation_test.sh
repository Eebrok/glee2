#!/bin/bash

export STRYKER_OUTPUT_DIRECTORY=./reports

run_stryker_test_on_directory(){
    test_directory="${1}"
    definition_directory=$(basename "${test_directory//.Specs}")

    printf "Found tests in ${test_directory} \n"
    printf "Definition directory: ${definition_directory} \n"

    cd "${test_directory}"
    dotnet stryker -r "['Html', 'ClearText']" \
        --project-file="${definition_directory}"/"${definition_directory}".csproj \
        --log-console info
    cd ..    
}

collect_report(){
    test_directory=$(basename "${1}")

    if [ -d "${test_directory}/StrykerOutput" ]; then
        mv "${test_directory}"/StrykerOutput "${STRYKER_OUTPUT_DIRECTORY}/${test_directory}"
    fi
}

main(){
    for directory in `find . -type d -name "*.Specs"`
    do
        run_stryker_test_on_directory "${directory}"
        collect_report "${directory}"
    done
}

main