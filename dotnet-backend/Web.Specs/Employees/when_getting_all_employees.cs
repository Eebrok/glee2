using System.Collections.Generic;
using Data.Projections;
using Domain.Employees;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Responses;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_getting_all_employees : given_an_employee_controller_context
    {
        Establish _context = () =>
        {
            //EE18:START
            _expectedResponse = Builder<EmployeeListing>.CreateListOfSize(2).Build();
            //EE18:END
            //RP18:START
            // _expectedResponse = Builder<Employee>.CreateListOfSize(2).Build();
            //RP18:END
            Mock.Get(_readOnlyRepository).Setup(x => x.Set()).Returns(_expectedResponse.AsAsyncQueryable());
        };

        Because of = async () => { _result = await _employeesController.GetAll(new Dictionary<string, string>()); };

        It should_return_all_existing_employee = () => { _result.Data.Should().Equal(_expectedResponse); };

        //EE19:START
        static IList<EmployeeListing> _expectedResponse;
        static EmployeesResponse _result;
        //EE19:END
        //RP19:START
        // static IList<Employee> _expectedResponse;
        // static IEnumerable<Employee> _result;
        //RP19:END
    }
}