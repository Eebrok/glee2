using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AcklenAvenue.Dispatch;
using Common.Exceptions;

namespace Common.Commands.Validators
{
    public class CompositeCommandValidator<TCommand> : ICommandValidator<TCommand>
    {
        private readonly IEnumerable<IValidationComponent<TCommand>> _validators;

        public CompositeCommandValidator(IEnumerable<IValidationComponent<TCommand>> validators)
        {
            _validators = validators;
        }

        public async Task Validate(TCommand command)
        {
            foreach (var validationComponent in _validators)
            {
                var validationResult = await validationComponent.Validate(command);

                if (validationResult.HasErrors)
                {
                    throw new NotValidException(validationResult.Errors);
                }
            }
        }
    }
}