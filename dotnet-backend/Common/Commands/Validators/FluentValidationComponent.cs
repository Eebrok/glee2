using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace Common.Commands.Validators
{
    public class FluentValidationComponent<TCommand> : AbstractValidator<TCommand>, IValidationComponent<TCommand>
    {
        public new async Task<IValidationResult> Validate(TCommand command)
        {
            var result = await ValidateAsync(command);

            return new CommandValidationResult(result.Errors.Select(failure => new ValidationError
            {
                Field = failure.PropertyName, 
                Message = failure.ErrorMessage, 
                FieldLabel = failure.PropertyName,
                Value = failure.AttemptedValue               
            }));
        }
    }
}