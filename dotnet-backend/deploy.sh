#!/usr/bin/env bash
set -x
set -e

echo "Deploying backend to ${EB_ENV_NAME} for review..."

jq < dockerrun.aws.template.json ".Image.Name=\"${CONTAINER_IMAGE}:${CI_BUILD_REF}\""  > Dockerrun.aws.json

cat Dockerrun.aws.json

export DB_NAME=$(echo "$EB_ENV_NAME" | tr '-' '_')
export DB_CONNECTION="Server=$DB_HOST;Database=$DB_NAME;User Id=$DB_USER;Password=$DB_PASSWORD;"

if [ ! -z "$(eb list | grep "${EB_ENV_NAME}")" ]
then
    echo "Updating existing environment"
    eb use "${EB_ENV_NAME}"
    eb setenv VERSION=1.0.0-${CI_COMMIT_REF_SLUG} \
        DB_CONNECTION="${DB_CONNECTION}" \
        TZ=${TZ}
    eb deploy "${EB_ENV_NAME}" --timeout 15 | tee "eb_deploy_output.txt"

else
    echo "Creating new environment"
    eb create "${EB_ENV_NAME}" \
        --single \
        --envvars VERSION=1.0.0-${CI_COMMIT_REF_SLUG},DB_CONNECTION="${DB_CONNECTION}",TZ=${TZ} --timeout 15 | tee "eb_deploy_output.txt"
fi

# Temporary hack to overcome issue with 'eb deploy' returning exit code 0 on error
# See http://stackoverflow.com/questions/23771923/elasticbeanstalk-deployment-error-command-hooks-directoryhooksexecutor-py-p

if grep -c -q -i error: "eb_deploy_output.txt"
then    
    cat eb_deploy_output.txt
    echo 'Error found in deploy log.'
    exit 1
fi
