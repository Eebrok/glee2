import { BaseEntity, getCurrentTimestamp } from './base.entity';

class Entity extends BaseEntity {}
describe('Base Entity', () => {
  it('should initialize Entity', () => {
    const entity = new Entity();
    expect(entity).toBeDefined();
  });

  it('should return current timestamp', () => {
    expect(getCurrentTimestamp()).toBe('CURRENT_TIMESTAMP');
  });
});
