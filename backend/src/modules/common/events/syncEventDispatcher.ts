import { EventBus } from '@nestjs/cqrs';
import { DomainEvent } from './domainEvent';

import { IEventDispatcher } from './eventDispatcher';
import { Injectable, Type } from '@nestjs/common';
import { IEventHandler } from './eventHandler';

export type EventHandlerMetatype = Type<IEventHandler<DomainEvent>>;

@Injectable()
export class SyncEventDispatcher implements IEventDispatcher {
  constructor(private eventBus: EventBus) {}
  publish<TEvent extends DomainEvent>(event: TEvent): Promise<void> {
    return new Promise(resolve => {
      this.eventBus.publish(event);
      resolve();
    });
  }

  register(handlers: EventHandlerMetatype[]): void {
    this.eventBus.register(handlers);
  }
}
