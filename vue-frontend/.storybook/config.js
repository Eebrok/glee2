import { addParameters, configure } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import '@storybook/addon-actions/register'
import '@storybook/addon-knobs/register'
import '@storybook/addon-links/register'
import '@storybook/addon-viewport/register'
import '@storybook/addon-console'

import crfTheme from './crfTheme.js'
import '@/assets/scss/index.scss'

// Import plugins.
import Vue from 'vue'
import Vuex from 'vuex'
import Vuelidate from 'vuelidate'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install plugins.
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Vuex)
Vue.use(Vuelidate)

Vue.component('nuxt-link', {
  props: ['to'],
  methods: {
    log() {
      action('link target')(this.to)
    }
  },
  template: '<a href="#" @click.prevent="log()"><slot>NuxtLink</slot></a>'
})

Vue.component('nuxt-link', {
  props: ['to'],
  methods: {
    log() {
      action('link target')(this.to)
    }
  },
  template: '<a href="#" @click.prevent="log()"><slot>NuxtLink</slot></a>'
})

Vue.component('b-nav-item', {
  props: ['href'],
  methods: {
    log() {
      if (this.href) {
        action('link target')(this.href)
      } else {
        action('clicked')()
      }
    }
  },
  template:
    '<a class="nav-link" href="#" @click.prevent="log()"><slot>NuxtLink</slot></a>'
})
Vue.component('b-dropdown-item', {
  props: ['href'],
  methods: {
    log() {
      if (this.href) {
        action('link target')(this.href)
      } else {
        action('clicked')()
      }
    }
  },
  template:
    '<li><a class="dropdown-item" href="#" @click.prevent="log()" > <slot>NuxtLink</slot></a ></li>'
})
Vue.component('b-navbar-brand', {
  props: ['href'],
  methods: {
    log() {
      action('link target')(this.href)
    }
  },
  template:
    '<a class="navbar-brand" href="#" @click.prevent="log()" > <slot>NuxtLink</slot></a >'
})

addParameters({ options: { theme: crfTheme } })

configure(require.context('../components/', true, /\.stories\.js$/), module)
