import moment from 'moment'
import { equals, pickAll, pathOr } from 'ramda'

export const formatDate = (
  date?:
    | string
    | number
    | void
    | moment.Moment
    | Date
    | (string | number)[]
    | moment.MomentInputObject
    | undefined,
  dateFormat?:
    | string
    | moment.MomentBuiltinFormat
    | (string | moment.MomentBuiltinFormat)[]
    | undefined,
  format: string | undefined = 'MM/DD/YYYY'
): string => moment(date, dateFormat).format(format)

export const havePropertiesChanged = (
  props: string[],
  original: unknown,
  changedObject: unknown
): boolean => !equals(pickAll(props, original), pickAll(props, changedObject))

export enum Translation {
  ADD_EMPLOYEE = 'Add employee',
  CANCEL = 'Cancel',
  YES = 'Yes',
  NO = 'No',
  DISCARD_CHANGES = 'Discard changes',
  ADDING_EMPLOYEE_SUCCESS = 'New employee has been added successfully!',
  ADDING_EMPLOYEE_FAIL = 'An error occurred while trying to add a new employee.',
  CANCEL_CHANGES_QUESTION = 'Are you sure you want to cancel?, all changes will be discarded.',
  CHANGES_DISCARD = 'All changes were discarded.',
  CHANGES_SAVED = 'All changes have been saved successfully!',
  EDIT_EMPLOYEE_FIELD_SUCCESS = 'Employee field has been saved.',
  EDIT_EMPLOYEE_FIELD_ERROR = 'There was an error updating the field.',
  GENERIC_ERROR = 'Oops!, something went wrong.',
  BAD_REQUEST_ERROR = 'Oops!, something went wrong, please check your data.',
  SERVER_ERROR = 'Oops!, something went wrong, we could not establish a connection to the server.'
}

export const getErrorMessage = (e: any) => {
  let errorMessage = pathOr<string>('', ['response', 'data', 'error'], e)
  if (!errorMessage)
    errorMessage = pathOr<string>(
      '',
      ['response', 'data', 'errors', '0', 'ErrorMessage'],
      e
    )

  return errorMessage
}
