import { Plugin } from '@nuxt/types'
import Vue from 'vue'
import { CombinedVueInstance } from 'vue/types/vue'
import createAuth0Client, {
  RedirectLoginResult,
  PopupLoginOptions,
  RedirectLoginOptions,
  GetTokenSilentlyOptions,
  GetTokenWithPopupOptions,
  LogoutOptions,
  Auth0Client,
  Auth0ClientOptions
} from '@auth0/auth0-spa-js'
import { Store } from 'vuex'

interface Data {
  loading: boolean
  isAuthenticated: boolean
  user?: Object
  auth0Client: Auth0Client | null
  // popupOpen: boolean
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  error: any | null
}

interface ProviderProps {
  onRedirectCallback: (result: RedirectLoginResult) => void
}

interface Methods {
  loginWithPopup(o: PopupLoginOptions): Promise<void>
  loginWithRedirect(o?: RedirectLoginOptions): Promise<void>
  logout(options?: LogoutOptions): void
  getTokenSilently(options?: GetTokenSilentlyOptions): Promise<string>
  getTokenWithPopup(options?: GetTokenWithPopupOptions): Promise<string>
  handleRedirectCallback(): Promise<RedirectLoginResult>
  load(): Promise<void>
}

export type Auth0Instance = CombinedVueInstance<
  Vue,
  Data,
  Methods,
  unknown,
  Record<never, any>
>

// eslint-disable-next-line prefer-const,import/no-mutable-exports
let instance: Auth0Instance | null = null

const useAuth0 = async (
  store: Store<any>,
  { onRedirectCallback, ...options }: ProviderProps & Auth0ClientOptions
) => {
  if (!instance) {
    const auth0Client = await createAuth0Client({
      ...options,
      redirect_uri: window.location.origin,
      cacheLocation: 'localstorage'
    })

    instance = new Vue<Data, Methods, unknown, Record<never, any>>({
      data(): Data {
        return {
          auth0Client: null,
          error: null,
          loading: true,
          isAuthenticated: false
        }
      },
      async created() {
        this.auth0Client = auth0Client

        if (
          // eslint-disable-next-line nuxt/no-globals-in-created
          window.location.search.includes('code=') &&
          // eslint-disable-next-line nuxt/no-globals-in-created
          window.location.search.includes('state=')
        ) {
          try {
            const { appState } = await this.handleRedirectCallback()
            onRedirectCallback(appState)
          } catch (e) {
            this.error = e
          }
        } else {
          await this.load()
        }
      },
      methods: {
        async loginWithPopup(options: PopupLoginOptions) {
          store.commit('auth0/setPopupOpen', true)

          try {
            await this.auth0Client!.loginWithPopup(options)
          } finally {
            store.commit('auth0/setPopupOpen', false)
          }

          store.commit('auth0/setUser', await this.auth0Client!.getUser())
          store.commit('auth0/setIsAuthenticated', true)
        },
        loginWithRedirect(options?: RedirectLoginOptions) {
          return this.auth0Client!.loginWithRedirect(options)
        },
        logout(options?: LogoutOptions) {
          options = {
            returnTo: window.location.origin
          }
          this.auth0Client!.logout(options)
          store.commit('auth0/setIsAuthenticated', false)
        },
        getTokenSilently(options: GetTokenSilentlyOptions) {
          return this.auth0Client!.getTokenSilently(options)
        },
        async getTokenWithPopup(options: GetTokenWithPopupOptions) {
          store.commit('auth0/setPopupOpen', true)

          try {
            const token = await this.auth0Client!.getTokenWithPopup(options)
            return token
          } finally {
            store.commit('auth0/setPopupOpen', false)
          }
        },
        async handleRedirectCallback(url?: string) {
          const result = await this.auth0Client!.handleRedirectCallback(url)

          await this.load()

          return result
        },
        async load() {
          this.isAuthenticated = await this.auth0Client!.isAuthenticated()
          store.commit('auth0/setIsAuthenticated', this.isAuthenticated)

          this.user = await this.auth0Client!.getUser()
          store.commit('auth0/setUser', this.user)

          if (this.isAuthenticated) {
            await this.auth0Client!.getTokenSilently()
          }
        }
      }
    })
  }
  return instance
}

const Auth0Plugin: Plugin = async (context, inject) => {
  const onRedirectCallback = (appState: any) =>
    context.app.router?.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    )

  const options = {
    domain: process.env.AUTH0_DOMAIN!,
    client_id: process.env.AUTH0_CLIENT_ID!,
    audience: process.env.AUTH0_AUDIENCE,
    scope: 'openid profile email',
    useRefreshTokens: true,
    onRedirectCallback
  }

  const $auth0 = await useAuth0(context.store, options)

  inject('auth0', $auth0)
  context.$auth0 = $auth0
}

export { instance }
export default Auth0Plugin
