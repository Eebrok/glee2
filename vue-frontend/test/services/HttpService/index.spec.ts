import { NuxtAxiosInstance } from '@nuxtjs/axios'
import querystring from 'query-string'
import { HttpService } from '@/services/HttpService'
import { Auth0Instance } from '~/plugins/Auth0'

const axiosMethods = {
  get: jest.fn(),
  post: jest.fn(),
  delete: jest.fn(),
  put: jest.fn()
}
const mockLoggedInValue = false
const authMethods = {
  loginWithRedirect: jest.fn(),
  getTokenSilently: jest.fn(),
  isAuthenticated: mockLoggedInValue
}
const useInterceptor = jest.fn()
const baseUrl = 'https://my-test-api.com'

describe('HttpService', () => {
  let nuxtAxiosInstance: NuxtAxiosInstance
  let authModule: Auth0Instance
  let httpService: HttpService
  const originalProcessEnv = { ...process.env }
  const url = 'some-request'
  const authHeaders = {
    headers: {
      Authorization: 'Bearer some_token-123',
      'Content-Type': 'application/json; charset=utf-8'
    }
  }

  beforeAll(() => {
    process.env.BACKEND_URL = baseUrl
  })
  afterAll(() => {
    process.env = { ...originalProcessEnv }
  })
  beforeEach(() => {
    jest.clearAllMocks()
    nuxtAxiosInstance = jest.fn<NuxtAxiosInstance, []>().mockReturnValue({
      // @ts-ignore
      create: jest.fn(() => ({
        interceptors: { response: { use: useInterceptor } },
        ...axiosMethods
      }))
    })()
    // @ts-ignore
    authModule = jest.fn<Auth0Instance, []>(() => ({ ...authMethods }))()
    authMethods.getTokenSilently.mockReturnValue(
      authHeaders.headers.Authorization.split(' ')[1]
    )
  })

  it('should initialize HttpService instance', () => {
    httpService = new HttpService(nuxtAxiosInstance, authModule)
    expect(httpService).toBeDefined()
  })
  it('should call axios interceptor response use method', () => {
    httpService = new HttpService(nuxtAxiosInstance, authModule)
    expect(useInterceptor).toHaveBeenCalled()
    expect(useInterceptor).toHaveBeenCalledWith(
      expect.any(Function),
      expect.any(Function)
    )
  })
  describe('when using service methods', () => {
    const mockResponse = { data: 'test' }
    beforeEach(() => {
      httpService = new HttpService(nuxtAxiosInstance, authModule)
    })
    describe('get', () => {
      const authHeaders = {
        headers: { Authorization: 'Bearer some_token-123' }
      }
      const url = 'some-request'
      beforeEach(() => {
        axiosMethods.get.mockReturnValueOnce(mockResponse)
        authMethods.getTokenSilently.mockReturnValueOnce(
          authHeaders.headers.Authorization.split(' ')[1]
        )
      })
      it('when no query params are specified', async () => {
        const res = await httpService.get(`/${url}`)

        expect(axiosMethods.get).toHaveBeenCalledTimes(1)
        expect(axiosMethods.get).toHaveBeenCalledWith(`${url}`, authHeaders)
        expect(res).toEqual(mockResponse.data)
      })
      it('when query params are specified', async () => {
        const params = { someData: '1' }

        const res = await httpService.get(`/${url}`, { params })

        expect(axiosMethods.get).toHaveBeenCalledTimes(1)
        expect(axiosMethods.get).toHaveBeenCalledWith(
          `${url}?${querystring.stringify(params)}`,
          authHeaders
        )
        expect(res).toEqual(mockResponse.data)
      })
    })
    describe('when token is required to make a request', () => {
      // @ts-ignore
      const hasToLoginSpy = jest.spyOn(HttpService.prototype, 'hasToLogin')
      beforeEach(() => {
        hasToLoginSpy.mockClear()
      })
      describe('put', () => {
        const testPayload = {
          some: 'payload'
        }
        beforeEach(() => {
          axiosMethods.put.mockReturnValueOnce(mockResponse)
        })
        it('when has to login returns true', async () => {
          hasToLoginSpy.mockReturnValueOnce(true)

          await httpService.put(`/${url}`, testPayload)

          expect(axiosMethods.put).not.toHaveBeenCalled()
          expect(authMethods.getTokenSilently).not.toHaveBeenCalled()
          expect(authMethods.loginWithRedirect).toHaveBeenCalled()
        })
        it('when has to login returns false', async () => {
          hasToLoginSpy.mockReturnValueOnce(false)

          const res = await httpService.put(`/${url}`, testPayload)

          expect(axiosMethods.put).toHaveBeenCalledTimes(1)
          expect(axiosMethods.put).toHaveBeenCalledWith(
            url,
            testPayload,
            authHeaders
          )
          expect(res).toEqual(mockResponse.data)
        })
      })
      describe('post', () => {
        const testPayload = {
          some: 'payload'
        }
        beforeEach(() => {
          axiosMethods.post.mockReturnValueOnce(mockResponse)
        })
        it('when has to login returns true', async () => {
          hasToLoginSpy.mockReturnValueOnce(true)

          await httpService.post(`/${url}`, testPayload)

          expect(axiosMethods.post).not.toHaveBeenCalled()
          expect(authMethods.getTokenSilently).not.toHaveBeenCalled()
          expect(authMethods.loginWithRedirect).toHaveBeenCalled()
        })
        it('when has to login returns false', async () => {
          hasToLoginSpy.mockReturnValueOnce(false)

          const res = await httpService.post(`/${url}`, testPayload)

          expect(axiosMethods.post).toHaveBeenCalledTimes(1)
          expect(axiosMethods.post).toHaveBeenCalledWith(
            url,
            testPayload,
            authHeaders
          )
          expect(res).toEqual(mockResponse.data)
        })
      })
      describe('delete', () => {
        beforeEach(() => {
          axiosMethods.delete.mockReturnValueOnce(mockResponse)
        })
        it('when has to login returns true', async () => {
          hasToLoginSpy.mockReturnValueOnce(true)

          await httpService.delete(`/${url}`)

          expect(axiosMethods.delete).not.toHaveBeenCalled()
          expect(authMethods.getTokenSilently).not.toHaveBeenCalled()
          expect(authMethods.loginWithRedirect).toHaveBeenCalled()
        })
        it('when has to login returns false', async () => {
          hasToLoginSpy.mockReturnValueOnce(false)

          const res = await httpService.delete(`/${url}`)

          expect(axiosMethods.delete).toHaveBeenCalledTimes(1)
          expect(axiosMethods.delete).toHaveBeenCalledWith(url, {
            headers: { Authorization: authHeaders.headers.Authorization }
          })
          expect(res).toEqual(mockResponse.data)
        })
      })
    })
    describe('trimSlashes', () => {
      it('should remove slashes from the beginning', () => {
        // @ts-ignore
        const res = httpService.trimSlashes('//some-request')
        expect(res).toBe('some-request')
      })
      it('should remove slashes from the end', () => {
        // @ts-ignore
        const res = httpService.trimSlashes('some-request//')
        expect(res).toBe('some-request')
      })
    })
    describe('getQueryStringFromParams', () => {
      it('should return query string', () => {
        const params = { id: 1, other: 'some' }
        // @ts-ignore
        const res = httpService.getQueryStringFromParams(params)
        expect(res).toBe('?id=1&other=some')
      })
    })
    describe('hasToLogin', () => {
      it('should return negated logged in value', () => {
        // @ts-ignore
        const res = httpService.hasToLogin()
        expect(res).toBe(!mockLoggedInValue)
      })
    })
    describe('onRejectedRequest', () => {
      const error = new Error('Some')
      it('should throw error if response is falsy', () => {
        expect(() =>
          // @ts-ignore
          httpService.onRejectedRequest(error)
        ).toThrow(error)
        expect(authMethods.loginWithRedirect).not.toHaveBeenCalled()
      })
      it('should throw error if response status is 400, 404 or 500', () => {
        const func = (error: Object) => () =>
          // @ts-ignore
          httpService.onRejectedRequest(error)

        expect(func({ response: { status: 500 } })).toThrow()
        expect(func({ response: { status: 404 } })).toThrow()
        expect(func({ response: { status: 400 } })).toThrow()
        expect(authMethods.loginWithRedirect).not.toHaveBeenCalled()
      })
      it('should call login method if status response code is 401', () => {
        const error = { response: { status: 401 } }
        expect(() =>
          // @ts-ignore
          httpService.onRejectedRequest(error)
        ).not.toThrow()
        expect(authMethods.loginWithRedirect).toHaveBeenCalled()
      })
    })
    describe('onFulfilledRequest', () => {
      it('should return response', () => {
        const data = { some: 1 }
        // @ts-ignore
        const res = httpService.onFulfilledRequest(data)
        expect(res).toEqual(data)
      })
    })
    describe('buildToken', () => {
      it('should return token', async () => {
        authMethods.getTokenSilently.mockReturnValueOnce(
          authHeaders.headers.Authorization.split(' ')[1]
        )
        // @ts-ignore
        const res = await httpService.buildToken()

        expect(authMethods.getTokenSilently).toHaveBeenCalled()
        expect(res).toEqual(authHeaders.headers.Authorization)
      })
      describe('when getTokenSilently method throws', () => {
        it('should throw when not login_required error and needToken is true', async () => {
          expect.assertions(1)
          const mockError = {
            error: 'login_required'
          }
          authMethods.getTokenSilently.mockRejectedValue(mockError)

          try {
            // @ts-ignore
            await httpService.buildToken(true)
          } catch (error) {
            expect(error).toEqual(mockError)
          }
        })
        it('should return empty string', async () => {
          const mockError = {
            error: 'login_required'
          }
          authMethods.getTokenSilently.mockRejectedValue(mockError)

          // @ts-ignore
          const token = await httpService.buildToken()
          expect(token).toEqual('Bearer ')
        })
      })
    })
  })
})
