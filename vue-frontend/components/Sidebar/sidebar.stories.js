import Vue from 'vue'
import { withKnobs } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import Sidebar from './Sidebar.vue'

Vue.prototype.$auth0 = {
  logout() {
    action('logout')()
  }
}

export default { title: 'Sidebar', decorator: [withKnobs], parameters: {} }

export const asAComponent = () => ({
  components: { Sidebar },
  template: `
    <Sidebar />
  `
})
