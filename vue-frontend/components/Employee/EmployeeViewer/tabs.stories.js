import { withKnobs } from '@storybook/addon-knobs'
import Tabs from './Tabs.vue'

export default {
  title: 'Tabs',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { Tabs },
  methods: {
    setActiveTab() {}
  },
  template: `
      <Tabs @active="setActiveTab" />
  `
})
