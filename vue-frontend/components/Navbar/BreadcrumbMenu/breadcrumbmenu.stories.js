import { withKnobs } from '@storybook/addon-knobs'
import BreadcrumbMenu from './BreadcrumbMenu.vue'

export default {
  title: 'BreadcrumbMenu',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { BreadcrumbMenu },
  data() {
    return {
      menu: [
        {
          text: 'Employees',
          style: this.thirdLevelBreadcrumb
        },
        {
          text: 'View & Manage',
          style: this.thirdLevelBreadcrumb
        }
      ]
    }
  },
  template: `
    <BreadcrumbMenu :menu="menu"/>
  `
})
