import { withKnobs } from '@storybook/addon-knobs'
import Navbar from './Navbar.vue'

export default {
  title: 'Navbar',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { Navbar },
  data() {
    return {
      breadcrumbMenu: [
        {
          text: 'Employees',
          style: this.thirdLevelBreadcrumb
        },
        {
          text: 'View & Manage',
          style: this.thirdLevelBreadcrumb
        }
      ],
      pageTitle: 'View & Manage Employees'
    }
  },
  template: `
    <Navbar :page-title="pageTitle" :breadcrumb-menu="breadcrumbMenu"/>
  `
})
