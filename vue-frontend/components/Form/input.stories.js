import { withKnobs, text } from '@storybook/addon-knobs'
import Input from './Input.vue'

export default { title: 'Input', decorator: [withKnobs], parameters: {} }

export const asAComponent = () => ({
  components: { Input },
  props: {
    label: { default: text('label', 'Some Label') }
  },
  template: `
    <Input :label="label" />
  `
})
