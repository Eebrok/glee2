#! /bin/bash

function set_name_prefix()
{
    NAME_PREFIX="${NAME_PREFIX:0:18}"
    NAME_PREFIX="${NAME_PREFIX//-_}"
    NAME_PREFIX=$(echo "$NAME_PREFIX" | tr '[:upper:]' '[:lower:]')
    export NAME_PREFIX=${NAME_PREFIX%"-"}
}

function set_staging_vars()
{
    if [ "${CI_COMMIT_REF_NAME}" = "staging" ]; then
        export RDS_DB_HOST=${RDS_DB_HOST_STAG}
    fi
}

function map_auth0_vars()
{
    export AUTH0_FE_CLIENT_ID=$AUTH0_FE_CLIENT_ID_DEV
    export AUTH0_DOMAIN=$AUTH0_DOMAIN_DEV
    export AUTH0_AUDIENCE=$AUTH0_AUDIENCE_DEV
    export AUTH0_BE_CLIENT_ID=$AUTH0_BE_CLIENT_ID_DEV
    export AUTH0_BE_CLIENT_SECRET=$AUTH0_BE_CLIENT_SECRET_DEV
}

# Calling the functions
set_name_prefix
map_auth0_vars
set_staging_vars