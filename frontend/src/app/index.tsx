import React, { useEffect } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import { useAuth0 } from '@auth0/auth0-react';
import './style.local.css';
import App from './containers/App';
import { Error } from './components';
import { Loading } from './components/Loading';
import { HttpService } from './services';
import ProtectedRoute from 'app/components/ProtectedRoute';

export let httpServiceInstance: HttpService;

export interface Props {}

const Root: React.FC<Props> = () => {
  const auth0 = useAuth0();
  const history = useHistory();
  useEffect(() => {
    httpServiceInstance = new HttpService(auth0);
    if (!window.localStorage.getItem('backendUrl')) {
      window.localStorage.setItem('backendUrl', process.env.API_URL || '');
    }
  }, [auth0]);
  if (auth0.isLoading) {
    return <Loading />;
  }
  if (auth0.error) {
    history.push('/error');
    return null;
  }

  const Logout = () => {
    auth0.logout({ returnTo: window.location.origin });
    return null;
  };
  const Login = () => {
    auth0.loginWithRedirect();
    return null;
  };

  return (
    <Switch>
      <Route exact path='/logout' component={Logout} />
      <Route exact path='/login' component={Login} />
      <Route exact path='/error' component={Error} />
      <ProtectedRoute component={App} />
    </Switch>
  );
};

export default hot(module)(Root);
