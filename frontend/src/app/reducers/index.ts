import { combineReducers } from 'redux';
import { History } from 'history';
import { connectRouter, RouterState } from 'connected-react-router';
import { employeeReducer } from '../containers/Employee/reducer/employees';
import { EmployeeState } from 'app/containers/Employee/reducer/state';

export interface RootState {
  router: RouterState;
  employees: EmployeeState;
}
// NOTE: current type definition of Reducer in 'react-router-redux' and 'redux-actions' module
// doesn't go well with redux@4
export const rootReducer = (history: History) =>
  combineReducers<RootState>({
    router: connectRouter(history),
    employees: employeeReducer,
  });
