const configure = require('@storybook/react').configure;

if (typeof require.context === 'undefined') {
  const path = require('path');
  const fs = require('fs');
  require.context = (base = '../src', scanSubDirectories = true, regularExpression = /\.stories\.tsx$/) => {
    const files = {};
    function readDirectory(directory) {
      fs.readdirSync(directory).forEach((file) => {
        const fullPath = path.resolve(directory, file);
        if (fs.statSync(fullPath).isDirectory()) {
          if (scanSubDirectories) readDirectory(fullPath);
          return;
        }
        if (!regularExpression.test(fullPath)) return;
        files[fullPath] = true;
      });
    }
    readDirectory(path.resolve(__dirname, base));
    function Module(file) {
      return require(file);
    }
    Module.keys = () => Object.keys(files);
    return Module;
  };
}
// // automatically import all files ending in *.stories.tsx
configure(require.context('../src', true, /\.stories\.tsx?$/), module);
